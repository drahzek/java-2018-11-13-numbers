package com.drahzek;

/* imported stuff */
import java.util.Scanner;

/**
 * exercise2Main class for the application.
 * @author Dominik Świerzyński
 */
public class exercise3Main {
    public static void main(String[] args) {

        /* scanner feature for future input */
        Scanner scanner = new Scanner(System.in);

        /* user provides the app with a value to convert */
        System.out.println("Podaj liczbę całkowitą do konwersji na binarną : ");
        int value = scanner.nextInt();

        /*if value is equal or between 0 and 15, prints out the converted value + text*/
        if (value >= 0 && value <= 15) {
            System.out.print(value + " binarnie = ");
            binaryConvert(value);
        }
        /* if value is outside range, prints out error*/
        else {
            System.out.println("Błąd. Podaj liczbę w zakresie od 0 do 15");
        }
    }

    /**
     * method responsible for the converting process, different approach from the one used last time
     */
    private static void binaryConvert(int value) {
        /* declaring array with 4 bits of space */
        int[] binArray = new int[4];
        /* declaring counter that will help us with keeping the 4 bits in check */
        int counter = 4;

        while (counter > 0) {

            if (value > 0) {
                /* every position of the array is declared to be a remainder from an input value divided by 2.
                Counter variable is lowered by one because array positions are 0-3 and it wouldn't point out the right place*/
                binArray[counter-1] = value % 2;

                /*after that, the value is divided by 2 and we go back to the main loop*/
                value = value / 2;
            }

            else {
                /* if value is not bigger than 0, then the position of an array is declared to be 0.
                That way we can finally store leading 0s as well! */
                binArray[counter-1] = 0;
            }
            /* when the if is done and the value is declared on the array, counter is decreased by one
             and we repeat the process until counter is equal to 0 */
            counter--;
        }

        /*prints out the value that is stored on 4 bits inside the array */
        System.out.println(binArray[0] + "" + binArray[1] + "" + binArray[2] + "" + binArray[3]);
    }
}
