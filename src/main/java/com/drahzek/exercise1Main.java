package com.drahzek;

//imported stuff
import java.util.Scanner;

/**
 * exercise1Main class for the application.
 * @author Dominik Świerzyński
 */
public class exercise1Main {
    public static void main(String[] args) {

        /* scanner feature for future input */
        Scanner inputScanner = new Scanner(System.in);

        /* running variable for infinite loop that can be terminated by user command later on */
        boolean running = true;

        /*initial output with instructions */
        System.out.println("Apka numeryczna");
        printUsage();

        /* infinite loop containing the main features and functionalities of the app */
        while (running) {
            System.out.println("Podaj liczbę całkowitą");
            int command = inputScanner.nextInt();

            switch (command) {
                case 42: {
                    /*the answer for everything, ceasing the activity after printing it out*/
                    System.out.println(command + " - to odpowiedź na wszystko, Sayonara!");
                    running = false;
                    break;
                }
                default: {
                    printUsage();
                    break;
                }
            }
        }
    }

    /**
     * Prints instructions for the user
     */
    private static void printUsage() {
        System.out.println("Poprawne komendy to : \n" +
                "42 - odpowiedź na wszystko \n");
    }
}
