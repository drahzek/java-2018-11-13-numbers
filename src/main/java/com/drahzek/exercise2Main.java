package com.drahzek;

/* imported stuff */
import java.util.Scanner;

/**
 * exercise2Main class for the application.
 * @author Dominik Świerzyński
 */
public class exercise2Main {
    public static void main(String[] args) {

        /* scanner feature for future input */
        Scanner scanner = new Scanner(System.in);

        /* generates table based on user input */
        System.out.println("Podaj wielkość tabeli : ");
        int size = scanner.nextInt();
        int[] testTable = new int[size];

        /* user provides the elements for the generated table */
        for(int i = 0; i<testTable.length; i++) {
            System.out.println("Podaj liczbę całkowitą dla pozycji " + (i+1) + " :");
            int Value = scanner.nextInt();
            testTable[i] = Value;
        }

        /* methods made for the sake of cleaning the code that was rather messy */
        printValues(testTable);
        findMinMax(testTable);

    }

    /**
     * Printer for the values that were given to the array testTable
     */
    public static void printValues(int[] testTable) {
        for (int i : testTable) {
            /* prints values in one line, divided by spaces */
            System.out.print(i + " ");
        }
        /* empty printliner that prevents printing right after the values */
        System.out.println("");
        System.out.println("To już cała tabela");
    }

    /**
     * finder of maximal and minimal value among those in the array
     */
    private static void findMinMax(int[] testTable) {
        int min = testTable[0];
        int max = testTable[0];

        for(int i=0; i<testTable.length; i++){
            if(testTable[i]<min){
                min=testTable[i];
            }
            else if(testTable[i]>max){
                max=testTable[i];
            }
        }
        System.out.println("Wartość minimalna = "+ min);
        System.out.println("Wartość maksymalna = "+ max);
    }
}
